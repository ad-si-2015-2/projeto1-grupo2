# Definição de Classes

## Servidor

1. Cria *Socket* de recebimento de conexão;
2. Recebe a *conexão* do lutador;
3. Cria uma *Thread* para cada conexão;
4. Adiciona o lutador à lista de jogadores na Classe Jogo;


## ConexãoThread

1. *Thread* de cada lutador;
2. *Recebe* e *envia* mensagens para o lutador;
3. Envia as mensagem recebis para a classe Jogo resolver a lógica;


## Jogo

1. Classe que irá conter toda a *lógica* do jogo;
2. Responsável por determinar de quem e a vez de joga;
3. Responsável por determinar quem é o vencedor;

## Mensageiro

1. Classe que contem a logica de envio de mensagens
2. Responsavel por receber mensagem
3. Responsavel por enviar a mensagem

# Definição de Model ( Regra de negocio )

## Estado

1. Representa os estados possiveis do jogador

## Jogada

1. Representa as jogadas possiveis

## Lutador

1. Representa a entidade lutados e suas propriedades e comportamentos