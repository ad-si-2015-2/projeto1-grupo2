package luta.jogo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import luta.conexao.ConexaoThread;
import luta.conexao.Mensageiro;
import luta.model.Estado;
import luta.model.Jogada;
import luta.model.Lutador;

/**
 * @author Humberto Miranda
 */
public class Jogo {

  private int vez = 0;
  private List<ConexaoThread> jogadores = new ArrayList<>();

  private Mensageiro mensageiro;

  /**
   * Adiciona o lutador à lista de lutadores
   * @param jogador
   */
  public final void addLutador(ConexaoThread lutador) {
    jogadores.add(lutador);
  }

  public final List<ConexaoThread> getLutadores() {
    return Collections.unmodifiableList(jogadores);
  }

  /**
   * Verifica se h� dois lutadores conectados e inicia a Luta;
   */
  public void startLuta() {
    if (validateListSize()) {
      mostrarMenu();
      imprimeLifes();
      iniciaJogada();
    } else {
      enviarMensagem("Aguardando segundo lutador para iniciar a Luta...");
    }
  }

  private boolean validateListSize() {
    return jogadores.size() == 2 && jogadores.get(0).getLutador().getNome() != null && jogadores.get(1)
      .getLutador().getNome() != null;
  }

  private void mostrarMenu() {
    enviarMensagem("\r\n...O jogo sera iniciado...");
    enviarMensagem("\r\n" + "|====================================================|\r\n"
        + "|-----------------  Comandos do Jogo  ---------------|\r\n"
        + "|                                                    |\r\n"
        + "| ###################################################|\r\n"
        + "| #################                 #################|\r\n"
        + "| #################   C -> Chute    #################|\r\n"
        + "| #################   S -> Soco     #################|\r\n"
        + "| #################   D -> Defesa   #################|\r\n"
        + "| #################   E -> Esquiva  #################|\r\n"
        + "| #################                 #################|\r\n"
        + "| ###################################################|\r\n"
        + "|====================================================|\r\n");
  }

  /**
   * Imprime a quantidade de vida na tela para cada jogador na lista
   */
  private void imprimeLifes() {
    jogadores.forEach(lutador -> enviarMensagem("Life Lutador " + lutador.getLutador().getNome() + ": "
        + lutador.getLutador().getLife()));
  }

  public void enviarMensagem(String mensagem) {
    jogadores.forEach(lutador -> {
      mensageiro = new Mensageiro(lutador.getSocket());
      mensageiro.enviarMensagem(mensagem);
    });
  }

  private void iniciaJogada() {
    if (vez == 0) {
      mensageiro = new Mensageiro(jogadores.get(0).getSocket());
      mensageiro.enviarMsg("Sua vez " + jogadores.get(0).getLutador().getNome() + ": ");
      mensageiro = new Mensageiro(jogadores.get(1).getSocket());
      mensageiro.enviarMensagem("Aguarde sua vez...");
    } else {
      mensageiro = new Mensageiro(jogadores.get(1).getSocket());
      mensageiro.enviarMsg("Sua vez " + jogadores.get(1).getLutador().getNome() + ": ");
      mensageiro = new Mensageiro(jogadores.get(0).getSocket());
      mensageiro.enviarMensagem("Aguarde sua vez...");
    }
  }

  public void analisaJogada(String jogada, int numLutador) {
    int inimigo = 0;
    if (vez == 0) {
      inimigo = 1;
    }

    Lutador lutador = jogadores.get(inimigo).getLutador();
    mensageiro = new Mensageiro(jogadores.get(inimigo).getSocket());

    if (numLutador == vez) {
      for (Jogada j : Jogada.values()) {
        if (jogada.equals(j.getAtalho())) {
          if (jogada.equals("D")) {
            jogadores.get(numLutador).getLutador().setEstado(Estado.DEFESA.toString());
          } else {
            if (lutador.getEstado().equals(Estado.INERCIA)) {
              jogadores.get(inimigo).getLutador().receberGolpe(j.randomHitIncercia());
            } else {
              jogadores.get(inimigo).getLutador().receberGolpe(j.randomHitDefesa());
            }
            jogadores.get(numLutador).getLutador().setEstado(Estado.INERCIA.toString());
          }
          mensageiro.enviarMensagem(j.getMensagem());
        }
      }

      verificaDerrota(inimigo);
    }
  }

  private void verificaDerrota(int inimigo) {
    if (jogadores.get(inimigo).getLutador().getLife() <= 0) {
      mensageiro.enviarMensagem("Que pena voce perdeu );");
      mensageiro = new Mensageiro(jogadores.get(vez).getSocket());
      mensageiro.enviarMensagem("Parabens voce venceu a luta!!");
    } else {
      imprimeLifes();
      setVez();
      iniciaJogada();
    }
  }

  private void setVez() {
    if (vez > 0) {
      vez = 0;
    } else {
      vez = 1;
    }
  }
}
