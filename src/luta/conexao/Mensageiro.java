package luta.conexao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Mensageiro {

  private BufferedReader entrada;
  private DataOutputStream saida;

  public Mensageiro(final Socket socket) {
    try {
      entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      saida = new DataOutputStream(socket.getOutputStream());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void enviarMensagem(String mensagem) {
    enviarMsg(mensagem + "\r\n");
  }

  public void enviarMsg(String mensagem) {
    try {
      saida.writeBytes(mensagem);
    } catch (IOException e) {
    }
  }

  public String receberMensagem() {
    try {
      return entrada.readLine();
    } catch (IOException e) {
    }
    return null;
  }
}
