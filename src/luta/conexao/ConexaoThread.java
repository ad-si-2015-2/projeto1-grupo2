package luta.conexao;

import java.net.Socket;

import luta.jogo.Jogo;
import luta.model.Jogada;
import luta.model.Lutador;

/**
 * @author Humberto Miranda
 */
public class ConexaoThread implements Runnable {

  private Socket socket;
  private Jogo jogo;
  private Mensageiro mensageiro;
  private Lutador lutador;

  /**
   * @param conexao
   * @param jogo
   * @throws Exception
   */
  public ConexaoThread(Socket conexao, Jogo jogo) {
    socket = conexao;
    this.jogo = jogo;
    mensageiro = new Mensageiro(conexao);
  }

  /**
   * Inicio de uma Thread propriamente dita;
   **/
  @Override
  public void run() {
    mensageiro.enviarMensagem("Voce esta conectado...");
    mensageiro.enviarMsg("Digite seu nome: ");
    lutador = new Lutador(mensageiro.receberMensagem(), jogo.getLutadores().size() - 1);
    jogo.startLuta();
    try {
      while (true) {
        boolean golpeValido = false;
        String jogada = mensageiro.receberMensagem().toUpperCase().trim();
        for (Jogada j : Jogada.values()) {
          if (jogada.equals(j.getAtalho())) {
            golpeValido = true;
          }
        }
        if (golpeValido) {
          jogo.analisaJogada(jogada, lutador.getNumLutador());
        } else {
          mensageiro.enviarMsg("Golpe invalido. Tente outro: ");
        }
      }
    } catch (NullPointerException e) {
      jogo.enviarMensagem("\nO seu oponente se desconectou do servidor =/");
    }
  }

  public Socket getSocket() {
    return socket;
  }

  public Lutador getLutador() {
    return lutador;
  }
}
