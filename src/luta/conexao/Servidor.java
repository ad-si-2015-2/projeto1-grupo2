package luta.conexao;

import java.net.ServerSocket;
import java.net.Socket;

import luta.jogo.Jogo;

/**
 * @author Humberto Miranda
 */
public class Servidor {

  private static ServerSocket servidor;
  private static Jogo jogo;

  /* M�todo que ser� iniciado no servidor; Abre conex�o para que os lutadores
   * conectem; */
  public static void main(String[] args) throws Exception {
    int porta = 5678;
    servidor = new ServerSocket(porta);
    jogo = new Jogo();

    System.out.println("Servidor Iniciado!");

    while (true) {
      Socket conexao = servidor.accept();
      ConexaoThread lutador = new ConexaoThread(conexao, jogo);
      jogo.addLutador(lutador);
      Thread lutadorThread = new Thread(lutador);
      lutadorThread.start();
    }
  }
}
