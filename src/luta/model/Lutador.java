package luta.model;

/**
 * @author Humberto Miranda
 * @author Dayan Costa
 */
public class Lutador {

  private String nome = null;
  private float life;
  private Estado estado = Estado.INERCIA;
  private int numLutador;

  public Lutador(String nome, int numLutador) {
	this.nome = nome;
	this.numLutador = numLutador;
	life = 100;
  }	

  public void setLife(float life) {
	this.life = life;
  }

  public float getLife() {
    return life;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Estado getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = Estado.valueOf(estado);
  }

  public int getNumLutador() {
    return numLutador;
  }

  public void setNumLutador(int numLutador) {
    this.numLutador = numLutador;
  }

  public void receberGolpe(float hit) {
    setLife(life - hit);
  }

}
