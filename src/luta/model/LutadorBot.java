package luta.model;

public class LutadorBot extends Lutador {

  public LutadorBot() {
    super("BOT", 1);
  }

  public String getJogada() {
    return Jogada.random();
  }

}
