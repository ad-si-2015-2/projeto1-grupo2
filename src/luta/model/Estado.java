package luta.model;

/**
 * Enum que representa os estados possíveis dos jogadores.
 * @author Dayan Costa
 */
public enum Estado {

  DEFESA,
  INERCIA;

}
